from utils.fixtures import *
from implementation import *


@pytest.mark.usefixtures('bear_variant')
class TestDelete:
    def test_delete_bear(self, bear_variant):
        bear = create_bear(generate_bear(bear_type=bear_variant))
        delete_bear(bear['bear_id'])
        assert_bear_doesnt_exist(bear['bear_id'])
