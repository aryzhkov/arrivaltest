from utils.fixtures import *
from implementation import *


@pytest.mark.usefixtures('bear_variant')
class TestCreate:
    def test_create_bear_with_invalid_age(self, bear_variant):
        new_bear = create_bear(generate_bear(bear_type=bear_variant, age=-1))
        assert_bear(new_bear['bear_id'], new_bear)


