from utils.fixtures import *
from implementation import *


class TestCreate:
    def test_create_bear_with_invalid_type(self):
        new_bear = create_bear(generate_bear(bear_type='PLUSH'))
        bear = create_bear(new_bear)
        assert_error(bear)


