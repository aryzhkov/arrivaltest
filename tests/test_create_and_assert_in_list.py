from utils.fixtures import *
from implementation import *


@pytest.mark.usefixtures('bear_variant')
@pytest.mark.usefixtures('create_everybear')
class TestCreate:
    def test_create_bear_and_assert_in_list(self, bear_variant):
        new_bear = create_bear(generate_bear(bear_type=bear_variant))
        assert_bear_in_list(new_bear['bear_id'], new_bear)


