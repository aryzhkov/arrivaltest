from utils.fixtures import *
from implementation import *


class TestCreateIncomplete:
    def test_create_bear_without_name(self):
        new_bear = generate_bear()
        del new_bear['bear_name']
        bear = create_bear(new_bear)
        assert_error(bear)

    def test_create_bear_without_type(self):
        new_bear = generate_bear()
        del new_bear['bear_type']
        bear = create_bear(new_bear)
        assert_error(bear)

    def test_create_bear_without_age(self):
        new_bear = generate_bear()
        del new_bear['bear_age']
        bear = create_bear(new_bear)
        assert_error(bear)


