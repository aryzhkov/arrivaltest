from utils.fixtures import *
from implementation import *


@pytest.mark.usefixtures('bear_variant')
@pytest.mark.usefixtures('create_everybear')
class TestDelete:
    def test_delete_bear_and_assert_in_list(self, bear_variant):
        bear = create_bear(generate_bear(bear_type=bear_variant))
        delete_bear(bear['bear_id'])
        assert_bear_doesnt_exist_in_list(bear['bear_id'])
