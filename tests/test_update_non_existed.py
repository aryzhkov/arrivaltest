from utils.fixtures import *
from implementation import *


@pytest.mark.usefixtures('bear_variant')
class TestUpdate:
    def test_update_non_existed_bear(self, bear_variant):
        bear = create_bear(generate_bear(bear_type=bear_variant))
        new_bear_data = generate_bear(bear_type=get_another_type(bear['bear_type']))
        new_bear_data['bear_id'] = bear['bear_id']+1
        data = update_bear(new_bear_data)
        assert_error(data)


