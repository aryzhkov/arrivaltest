import pytest
from implementation import *
from data.bear_data import Bear


@pytest.fixture(params=Bear.available_types())
def bear_variant(request):
    return request.param


@pytest.fixture
def create_everybear():
    for bear_type in Bear.available_types():
        create_bear(generate_bear(bear_type=bear_type))


@pytest.fixture(autouse=True)
def cleanup():
    yield
    bears = get_bears()
    for bear in bears:
        delete_bear(bear['bear_id'])

