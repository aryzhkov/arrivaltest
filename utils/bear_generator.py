import names
import random
from data.bear_data import Bear


def generate_name():
    return names.get_first_name().upper()


def generate_bear(bear_type: str = None, name: str = None, age: float = None):
    bear = {
        "bear_type": bear_type if bear_type else random.choice(Bear.available_types()),
        "bear_name": name if name else generate_name(),
        "bear_age": age if age else float('{0:.1f}'.format(random.uniform(*Bear.age_range())))
    }
    return bear


def get_another_type(current_type):
    available_types = [bear_type for bear_type in Bear.available_types() if bear_type != current_type]
    return random.choice(available_types)
