class Bear:
    @staticmethod
    def available_types():
        return ['POLAR', 'BROWN', 'BLACK', 'GUMMY']

    @staticmethod
    def age_range():
        return 0.1, 50
