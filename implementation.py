import requests
import os
import json
import re
from utils.bear_generator import *

url = f'{os.getenv("HOST")}/bear'


def get_bears():
    response = requests.get(url)
    if response.status_code != 200:
        return {'Error': f'Cannot get bear list: {response.reason}'}
    return response.json()


def get_bear(bear_id: int):
    response = requests.get(f'{url}/{bear_id}')
    if response.status_code != 200:
        return {'Error': f'Cannot get bear with id {bear_id}: {response.reason}'}
    return response.json() if response.text != 'EMPTY' else None


def create_bear(bear: dict = None):
    data = bear if bear else generate_bear()
    response = requests.post(url, data=json.dumps(data))
    if response.status_code != 200:
        return {'Error': f'Cannot create bear {data}: {response.reason}'}
    try:
        data['bear_id'] = int(response.text)
    except ValueError:
        data = response.text
    return data


def update_bear(bear: dict):
    response = requests.put(f"{url}/{bear['bear_id']}", json.dumps(bear))
    if response.status_code != 200:
        return {'Error': f'Cannot update bear: {response.reason}'}
    return bear['bear_id']



def delete_bear(bear_id: int):
    response = requests.delete(f'{url}/{bear_id}')
    assert response.status_code == 200, f'Cannot delete bear with id {bear_id}: {response.reason}'


def count_bears():
    return len(get_bears())


def assert_bear(bear_id: int, expected_bear: dict):
    actual_bear = get_bear(bear_id)
    if actual_bear:
        errors = __assert_fields(actual_bear, expected_bear)
    else:
        errors = f"Bear {expected_bear} doesn't exist"
    assert not bool(errors), f"Something goes wrong: {errors}"


def assert_bear_in_list(bear_id: int, expected_bear: dict):
    actual_bear = __find_bear_in_list(bear_id)
    if actual_bear:
        errors = __assert_fields(actual_bear, expected_bear)
    else:
        errors = f'Bear {expected_bear} is not in list'
    assert not bool(errors), f"Something goes wrong: {errors}"


def assert_bear_doesnt_exist(bear_id: int):
    bear = get_bear(bear_id)
    assert not bear, f"Bear was not deleted: {bear}"


def assert_bear_doesnt_exist_in_list(bear_id: int):
    bear = __find_bear_in_list(bear_id)
    assert not bear, f"Bear was not deleted: {bear}"


def assert_error(data: dict):
    assert 'Error' in data


def __find_bear_in_list(bear_id: int):
    try:
        actual_bear: dict = next(item for item in get_bears() if item['bear_id'] == bear_id)
    except StopIteration:
        actual_bear = None
    return actual_bear


def __assert_fields(actual: dict, expected: dict):
    errors = {}
    for key, value in expected.items():
        if key in actual:
            if value != actual[key]:
                errors[key] = f"Expected value: {value} — actual value {actual[key]}. "
        else:
            errors[key] = "Actual bear doesn't have that field"
    return errors
